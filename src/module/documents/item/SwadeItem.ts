import {
  Context,
  DocumentModificationOptions,
} from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs';
import { ChatMessageDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/chatMessageData';
import {
  ItemDataConstructorData,
  ItemDataSource,
} from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData';
import { EquipState, Updates } from '../../../globals';
import {
  ItemAction,
  TraitRollModifier,
} from '../../../interfaces/additional.interface';
import IRollOptions from '../../../interfaces/RollOptions.interface';
import RollDialog from '../../apps/RollDialog';
import { constants } from '../../constants';
import { Logger } from '../../Logger';
import * as util from '../../util';
import SwadeActor from '../actor/SwadeActor';
import {
  ItemChatCardAction,
  ItemChatCardChip,
  ItemChatCardData,
  ItemChatCardPowerPoints,
  UsageUpdates,
  UsageUpdatesContext,
} from './SwadeItem.interface';

declare global {
  interface DocumentClassConfig {
    Item: typeof SwadeItem;
  }
  interface FlagConfig {
    Item: {
      swade: {
        embeddedAbilities: [string, ItemDataSource][];
        embeddedPowers: [string, ItemDataSource][];
        [key: string]: unknown;
      };
    };
  }
}

export default class SwadeItem extends Item {
  overrides: DeepPartial<Record<string, string | number | boolean>> = {};

  static RANGE_REGEX = /[0-9]+\/*/g;

  constructor(data?: ItemDataConstructorData, context?: Context<SwadeActor>) {
    super(data, context);
    this.overrides = this.overrides ?? {};
  }

  get isMeleeWeapon(): boolean {
    if (this.type !== 'weapon') return false;
    const shots = this.system.shots;
    const currentShots = this.system.currentShots;
    return (!shots && !currentShots) || (shots === 0 && currentShots === 0);
  }

  get range() {
    //return early if the type doesn't match
    if (this.type !== 'weapon' && this.type !== 'power') return;
    //match the range string via Regex
    const match = this.system.range.match(SwadeItem.RANGE_REGEX);
    //return early if nothing is found
    if (!match) return;
    //split the string and convert the values to numbers
    const ranges = match.join('').split('/');
    //make sure the array is 4 values long
    const increments = Array.from(
      { ...ranges, length: 4 },
      (v) => Number(v) || 0,
    );
    return {
      short: increments[0],
      medium: increments[1],
      long: increments[2],
      extreme: increments[3] || increments[2] * 4,
    };
  }

  /**
   * @returns whether this item can be an arcane device
   */
  get canBeArcaneDevice(): boolean {
    return ['gear', 'armor', 'shield', 'weapon'].includes(this.type);
  }

  get isArcaneDevice(): boolean {
    if (!this.canBeArcaneDevice) return false;
    return getProperty(this.system, 'isArcaneDevice') as boolean;
  }

  get isPhysicalItem(): boolean {
    const types = [
      'weapon',
      'armor',
      'shield',
      'gear',
      'consumable',
      'container',
    ];
    return types.includes(this.type);
  }

  get isReadied(): boolean {
    const type = this.type;
    if (
      type === 'weapon' ||
      type === 'armor' ||
      type === 'shield' ||
      type === 'gear'
    ) {
      return this.system.equipStatus > constants.EQUIP_STATE.CARRIED;
    }
    return false;
  }

  get embeddedAbilities() {
    const flagContent = this.getFlag('swade', 'embeddedAbilities') ?? [];
    return new Map(flagContent);
  }

  get embeddedPowers() {
    const flagContent = this.getFlag('swade', 'embeddedPowers') ?? [];
    return new Map(flagContent);
  }

  static override migrateData(data) {
    super.migrateData(data);
    if (data.flags?.swade?.embeddedAbilities) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      for (const [key, item] of data.flags.swade.embeddedAbilities) {
        if (item.system && !item.data) continue;
        item.system = {
          ...item.data,
        };
        delete item.data;
      }
    }
    if (data.flags?.swade?.embeddedPowers) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      for (const [key, item] of data.flags.swade.embeddedPowers) {
        if (item.system && !item.data) continue;
        item.system = {
          ...item.data,
        };
        delete item.data;
      }
    }
    return data;
  }

  async rollDamage(options: IRollOptions = {}) {
    const modifiers = new Array<TraitRollModifier>();
    let itemData;
    if (['weapon', 'power', 'shield'].includes(this.type)) {
      itemData = this.system;
    } else {
      return null;
    }
    const label = this.name;
    let ap = getProperty(this.system, 'ap');

    if (ap) {
      ap = ` - ${game.i18n.localize('SWADE.Ap')} ${ap}`;
    } else {
      ap = ` - ${game.i18n.localize('SWADE.Ap')} 0`;
    }

    let rollParts = [itemData.damage];

    if (this.type === 'shield' || options.dmgOverride) {
      rollParts = [options.dmgOverride];
    }
    //Additional Mods
    if (options.additionalMods) {
      modifiers.push(...options.additionalMods);
    }

    const terms = Roll.parse(
      rollParts.join(''),
      this.parent?.getRollData() ?? {},
    );
    const baseRoll = new Array<string>();
    for (const term of terms) {
      if (term instanceof Die) {
        if (!term.modifiers.includes('x') && !term.options.flavor) {
          term.modifiers.push('x');
        }
        baseRoll.push(term.formula);
      } else if (term instanceof StringTerm) {
        baseRoll.push(this._makeExplodable(term.term));
      } else {
        baseRoll.push(term.expression);
      }
    }

    //Conviction Modifier
    if (
      this.parent?.type !== 'vehicle' &&
      game.settings.get('swade', 'enableConviction') &&
      this.parent?.system.details.conviction.active
    ) {
      modifiers.push({
        label: game.i18n.localize('SWADE.Conv'),
        value: '+1d6x',
      });
    }

    let flavour = '';
    if (options.flavour) {
      flavour = ` - ${options.flavour}`;
    }

    //Joker Modifier
    if (this.parent?.hasJoker) {
      modifiers.push({
        label: game.i18n.localize('SWADE.Joker'),
        value: '+2',
      });
    }

    const roll = new Roll(baseRoll.join(''));

    /**
     * A hook event that is fired before damage is rolled, giving the opportunity to programatically adjust a roll and its modifiers
     * @category Hooks
     * @param {SwadeActor} actor                The actor that owns the item which rolls the damage
     * @param {SwadeItem} item                  The item that is used to create the damage value
     * @param {Roll} roll                       The built base roll, without any modifiers
     * @param {TraitRollModifier[]} modifiers   An array of modifiers which are to be added to the roll
     * @param {IRollOptions} options            The options passed into the roll function
     */
    Hooks.call('swadeRollDamage', this.actor, this, roll, modifiers, options);

    if (options.suppressChat) {
      return Roll.fromTerms([
        ...roll.terms,
        ...Roll.parse(
          modifiers
            .map(util.normalizeRollModifiers)
            .reduce(util.modifierReducer, ''),
          this.getRollData(),
        ),
      ]);
    }

    // Roll and return
    return RollDialog.asPromise({
      roll: roll,
      mods: modifiers,
      speaker: ChatMessage.getSpeaker({ actor: this.actor! }),
      flavor: `${label} ${game.i18n.localize('SWADE.Dmg')}${ap}${flavour}`,
      title: `${label} ${game.i18n.localize('SWADE.Dmg')}`,
      item: this,
      flags: { swade: { colorMessage: false } },
    });
  }

  async setEquipState(state: EquipState): Promise<EquipState> {
    const equipState = constants.EQUIP_STATE;
    Logger.debug(
      `Trying to set state ${util.getKeyByValue(equipState, state)} on item ${
        this.name
      } with type ${this.type}`,
    );
    if (
      (this.type === 'weapon' && state === equipState.EQUIPPED) ||
      (this.type === 'consumable' && state > equipState.CARRIED)
    ) {
      Logger.warn('You cannot set this state on the item ' + this.name, {
        toast: true,
      });
      return this.system.equipStatus;
    }
    await this.update({ 'system.equipStatus': state });
    return state;
  }

  async getChatData(
    enrichOptions: Partial<TextEditor.EnrichOptions> = { async: true },
  ): Promise<ItemChatCardData> {
    // Item properties
    const chips = new Array<ItemChatCardChip>();
    const type = this.type;
    if (type === 'hindrance') {
      let label = game.i18n.localize('SWADE.Major');
      if (this.system.major) {
        label = game.i18n.localize('SWADE.Minor');
      }
      chips.push({ text: label });
    }
    if (type === 'shield') {
      if (this.isReadied) {
        chips.push({
          icon: '<i class="fas fa-tshirt"></i>',
          title: game.i18n.localize('SWADE.Equipped'),
        });
      } else {
        chips.push({
          icon: '<i class="fas fa-tshirt" style="color:grey"></i>',
          title: game.i18n.localize('SWADE.Unequipped'),
        });
      }
      chips.push(
        {
          icon: '<i class="fas fa-user-shield"></i>',
          text: this.system.parry,
          title: game.i18n.localize('SWADE.Parry'),
        },
        {
          icon: '<i class="fas fas fa-umbrella"></i>',
          text: this.system.cover,
          title: game.i18n.localize('SWADE.Cover._name'),
        },
        {
          icon: '<i class="fas fa-dumbbell"></i>',
          text: this.system.minStr,
        },
        {
          icon: '<i class="fas fa-sticky-note"></i>',
          text: await TextEditor.enrichHTML(this.system.notes, enrichOptions),
          title: game.i18n.localize('SWADE.Notes'),
        },
      );
    }
    if (type === 'armor') {
      for (const [location, covered] of Object.entries(this.system.locations)) {
        if (!covered) continue;
        chips.push({
          text: game.i18n.localize(
            `SWADE.${location.charAt(0).toUpperCase() + location.slice(1)}`,
          ),
        });
      }
      if (this.isReadied) {
        chips.push({
          icon: '<i class="fas fa-tshirt"></i>',
          title: game.i18n.localize('SWADE.Equipped'),
        });
      } else {
        chips.push({
          icon: '<i class="fas fa-tshirt" style="color:grey"></i>',
          title: game.i18n.localize('SWADE.Unequipped'),
        });
      }
      chips.push(
        {
          icon: '<i class="fas fa-shield-alt"></i>',
          title: game.i18n.localize('SWADE.Armor'),
          text: this.system.armor,
        },
        {
          icon: '<i class="fas fa-dumbbell"></i>',
          text: this.system.minStr,
        },
        {
          icon: '<i class="fas fa-sticky-note"></i>',
          text: await TextEditor.enrichHTML(this.system.notes, enrichOptions),
          title: game.i18n.localize('SWADE.Notes'),
        },
      );
    }
    if (type === 'edge') {
      chips.push({
        text: this.system.requirements.value,
      });
      if (this.system.isArcaneBackground) {
        chips.push({ text: game.i18n.localize('SWADE.Arcane') });
      }
    }
    if (type === 'power') {
      chips.push(
        {
          text: this.system.rank,
        },
        { text: this.system.arcane },
        {
          text: this.system.pp + game.i18n.localize('SWADE.PPAbbreviation'),
        },
        {
          icon: '<i class="fas fa-ruler"></i>',
          text: this.system.range,
          title: game.i18n.localize('SWADE.Range._name'),
        },
        {
          icon: '<i class="fas fa-shield-alt"></i>',
          text: this.system.ap,
          title: game.i18n.localize('SWADE.Ap'),
        },
        {
          icon: '<i class="fas fa-hourglass-half"></i>',
          text: this.system.duration,
          title: game.i18n.localize('SWADE.Dur'),
        },
        {
          text: this.system.trapping,
        },
      );
    }
    if (type === 'weapon') {
      if (this.isReadied) {
        chips.push({
          icon: '<i class="fas fa-tshirt"></i>',
          title: game.i18n.localize('SWADE.Equipped'),
        });
      } else {
        chips.push({
          icon: '<i class="fas fa-tshirt" style="color:grey"></i>',
          title: game.i18n.localize('SWADE.Unequipped'),
        });
      }
      chips.push(
        {
          icon: '<i class="fas fa-fist-raised"></i>',
          text: this.system.damage,
          title: game.i18n.localize('SWADE.Dmg'),
        },
        {
          icon: '<i class="fas fa-shield-alt"></i>',
          text: this.system.ap,
          title: game.i18n.localize('SWADE.Ap'),
        },
        {
          icon: '<i class="fas fa-user-shield"></i>',
          text: this.system.parry,
          title: game.i18n.localize('SWADE.Parry'),
        },
        {
          icon: '<i class="fas fa-ruler"></i>',
          text: this.system.range,
          title: game.i18n.localize('SWADE.Range._name'),
        },
        {
          icon: '<i class="fas fa-tachometer-alt"></i>',
          text: this.system.rof,
          title: game.i18n.localize('SWADE.RoF'),
        },
        {
          icon: '<i class="fas fa-sticky-note"></i>',
          text: await TextEditor.enrichHTML(this.system.notes, enrichOptions),
          title: game.i18n.localize('SWADE.Notes'),
        },
      );
    }

    //Additional actions
    const itemActions = getProperty(
      this.system,
      'actions.additional',
    ) as Record<string, ItemAction>;

    const actions = new Array<ItemChatCardAction>();
    for (const action in itemActions) {
      actions.push({
        key: action,
        type: itemActions[action].type,
        name: itemActions[action].name,
      });
    }

    const data: ItemChatCardData = {
      description: await TextEditor.enrichHTML(
        this.system.description,
        enrichOptions,
      ),
      chips: chips,
      actions: actions,
    };
    return data;
  }

  /** A shorthand function to roll skills directly */
  async roll(options: IRollOptions = {}) {
    //return early if there's no parent or this isn't a skill
    if (this.type !== 'skill' || !this.parent) return null;
    return this.parent.rollSkill(this.id, options);
  }

  /**
   * Assembles data and creates a chat card for the item
   * @returns the rendered chatcard
   */
  async show() {
    // Basic template rendering data
    if (!this.actor) return;
    const token = this.actor.token;

    const tokenId = token ? `${token.parent?.id}.${token.id}` : null;
    const ammoManagement = game.settings.get('swade', 'ammoManagement');
    const hasAmmoManagement =
      this.type === 'weapon' &&
      !this.isMeleeWeapon &&
      ammoManagement &&
      !getProperty(this.system, 'autoReload');
    const hasDamage = !!getProperty(this.system, 'damage');
    const hasTraitRoll =
      ['weapon', 'power', 'shield'].includes(this.type) &&
      !!getProperty(this.system, 'actions.skill');
    const hasReloadButton =
      ammoManagement &&
      this.type === 'weapon' &&
      getProperty(this.system, 'shots') > 0 &&
      !getProperty(this.system, 'autoReload');

    const additionalActions: Record<string, ItemAction> =
      getProperty(this.system, 'actions.additional') || {};

    const hasTraitActions = Object.values(additionalActions).some(
      (v) => v.type === 'skill',
    );
    const hasDamageActions = Object.values(additionalActions).some(
      (v) => v.type === 'damage',
    );

    const templateData = {
      actorId: this.parent?.id,
      tokenId: tokenId,
      item: this,
      data: await this.getChatData(),
      hasAmmoManagement,
      hasReloadButton,
      hasDamage,
      showDamageRolls: hasDamage || hasDamageActions,
      trait: getProperty(this.system, 'actions.skill'),
      hasTraitRoll,
      showTraitRolls: hasTraitRoll || hasTraitActions,
      powerPoints: this._getPowerPoints(),
      settingRules: {
        noPowerPoints: game.settings.get('swade', 'noPowerPoints'),
      },
    };

    // Render the chat card template
    const template = 'systems/swade/templates/chat/item-card.hbs';
    const html = await renderTemplate(template, templateData);

    // Basic chat message data
    const chatData: ChatMessageDataConstructorData = {
      user: game.user?.id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: html,
      speaker: {
        actor: this.parent?.id,
        token: token?.id,
        scene: token?.parent?.id,
        alias: this.parent?.name,
      },
      flags: { 'core.canPopout': true },
    };

    if (
      game.settings.get('swade', 'hideNpcItemChatCards') &&
      this.actor?.type === 'npc'
    ) {
      chatData.whisper = game.users!.filter((u) => u.isGM).map((u) => u.id!);
    }

    // Toggle default roll mode
    const rollMode = game.settings.get('core', 'rollMode');
    if (['gmroll', 'blindroll'].includes(rollMode))
      chatData.whisper = ChatMessage.getWhisperRecipients('GM').map(
        (u) => u.id!,
      );
    if (rollMode === 'selfroll') chatData.whisper = [game.user!.id!];
    if (rollMode === 'blindroll') chatData.blind = true;

    // Create the chat message
    const chatCard = await ChatMessage.create(chatData);
    Hooks.call('swadeChatCard', this.actor, this, chatCard, game.user!.id);
    return chatCard;
  }

  getTraitModifiers(): TraitRollModifier[] {
    const modifiers = new Array<TraitRollModifier>();
    if (getProperty(this.system, 'actions.skillMod')) {
      modifiers.push({
        label: game.i18n.localize('SWADE.ItemTraitMod'),
        value: getProperty(this.system, 'actions.skillMod'),
      });
    }
    if (this.type === 'weapon') {
      if (this.system.equipStatus === constants.EQUIP_STATE.OFF_HAND) {
        modifiers.push({
          label: game.i18n.localize('SWADE.OffHandPenalty'),
          value: -2,
        });
      }
      if (this.system.trademark > 0) {
        modifiers.push({
          label: game.i18n.localize('SWADE.TrademarkWeapon.Label'),
          value: '+' + this.system.trademark,
        });
      }
    }

    return modifiers;
  }

  async consume(charges = 1) {
    const useQuantity = this.type === 'consumable';
    const useResource = this.type === 'weapon';

    const usage = this._getUsageUpdates({
      charges,
      useQuantity,
      useResource,
    });
    if (!usage) return;

    /**
     * A hook event that is fired before an item is consumed, giving the opportunity to programmatically adjust the usage and/or trigger custom logic
     * @category Hooks
     * @param item               The item that is used being consumed
     * @param charges            The charges used.
     * @param usage              The determined usage updates that resulted from consuming this item
     */
    Hooks.call('swadePreConsumeItem', this, charges, usage);

    const { actorUpdates, itemUpdates, resourceUpdates } = usage;

    let updatedItems = new Array<StoredDocument<SwadeItem>>();
    // Persist the updates
    if (!foundry.utils.isEmpty(itemUpdates)) {
      await this.update(itemUpdates);
    }
    if (!foundry.utils.isEmpty(actorUpdates)) {
      await this.actor?.update(actorUpdates);
    }
    if (resourceUpdates.length) {
      updatedItems = (await this.actor?.updateEmbeddedDocuments(
        'Item',
        resourceUpdates,
      )) as Array<StoredDocument<SwadeItem>>;
    }

    /**
     * A hook event that is fired after an item is consumed but before cleanup happens
     * @category Hooks
     * @param item               The item that is used being consumed
     * @param charges            The charges used.
     * @param usage              The determined usage updates that resulted from consuming this item
     */
    Hooks.call('swadeConsumeItem', this, charges, usage);

    await this._postConsumptionCleanup(updatedItems);
  }

  protected async _postConsumptionCleanup(
    updatedItems: StoredDocument<SwadeItem>[],
  ) {
    const shouldDelete = (item: SwadeItem) => {
      return (
        item?.type === 'consumable' &&
        item.system.destroyOnEmpty &&
        item.system.quantity === 0 &&
        item.isOwned
      );
    };

    for (const update of updatedItems) {
      const item = this.parent?.items.get(update.id);
      if (item && shouldDelete(item)) {
        await item.delete();
      }
    }
    if (shouldDelete(this)) {
      await this.delete();
    }
  }

  protected _getUsageUpdates({
    charges,
    useQuantity,
    useResource,
  }: UsageUpdatesContext): UsageUpdates | false {
    const actorUpdates: Updates = {};
    const itemUpdates: Updates = {};
    const resourceUpdates = new Array<Updates>();

    if (useQuantity) {
      const canConsume = this._handleUseConsumable(charges, itemUpdates);
      if (canConsume === false) return false;
    }

    if (useResource) {
      const canConsume = this._handleConsumeResource(
        charges,
        itemUpdates,
        resourceUpdates,
      );
      if (canConsume === false) return false;
    }

    return { actorUpdates, itemUpdates, resourceUpdates };
  }

  protected _handleUseConsumable(
    chargesToUse: number,
    itemUpdates: Updates,
  ): void | boolean {
    //type guard
    if (this.type !== 'consumable') return false;

    //gather variables
    const currentCharges = this.system.charges.value;
    const maxCharges = this.system.charges.max;
    const quantity = this.system.quantity;
    const maxChargesOnStack = (quantity - 1) * maxCharges + currentCharges;

    //abort early if too much is being used
    if (chargesToUse > maxChargesOnStack) return false;

    const totalRemainingCharges = maxChargesOnStack - chargesToUse;
    const newQuantity = Math.ceil(totalRemainingCharges / maxCharges);
    let newCharges = totalRemainingCharges % maxCharges;

    if (newCharges === 0 && newQuantity < quantity && newQuantity !== 0) {
      newCharges = maxCharges;
    }

    //write updates
    itemUpdates['system.quantity'] = Math.max(0, newQuantity);
    itemUpdates['system.charges.value'] = newCharges;
  }

  private _handleConsumeResource(
    chargesToUse: number,
    itemUpdates: Updates,
    resourceUpdates: Updates[],
  ): void | boolean {
    if (this.type === 'weapon') {
      if (this.system.autoReload) {
        const ammo = this.parent?.items.getName(this.system.ammo);
        const quantity = ammo?.system['quantity'];
        if (!ammo || chargesToUse > quantity) {
          Logger.warn('SWADE.NotEnoughAmmo', { toast: true, localize: true });
          return false;
        }
        resourceUpdates.push({
          _id: ammo.id,
          'data.quantity': quantity - chargesToUse,
        });
      } else {
        const currentShots = this.system.currentShots;
        const usesShots = !!this.system.shots && !!currentShots;
        if (!usesShots || chargesToUse > currentShots) {
          Logger.warn('SWADE.NotEnoughAmmo', { toast: true, localize: true });
          return false;
        }
        itemUpdates['data.currentShots'] = currentShots - chargesToUse;
      }
    }
  }

  private _makeExplodable(expression: string): string {
    // Make all dice of a roll able to explode
    const diceRegExp = /\d*d\d+[^kdrxc]/g;
    expression = expression + ' '; // Just because of my poor reg_exp foo
    const diceStrings: string[] = expression.match(diceRegExp) || [];
    const used = new Array<string>();
    for (const match of diceStrings) {
      if (used.indexOf(match) === -1) {
        expression = expression.replace(
          new RegExp(match.slice(0, -1), 'g'),
          match.slice(0, -1) + 'x',
        );
        used.push(match);
      }
    }
    return expression;
  }

  /** @returns the power points for the AB that this power belongs to or null when the item is not a power */
  private _getPowerPoints(): ItemChatCardPowerPoints | null {
    if (this.type === 'power') {
      const actor = this.parent!;

      let value: number = getProperty(actor.system, 'powerPoints.value');
      let max: number = getProperty(actor.system, 'powerPoints.max');
      const arcane = this.system.arcane;
      if (arcane) {
        value = getProperty(actor.system, `powerPoints.${arcane}.value`);
        max = getProperty(actor.system, `powerPoints.${arcane}.max`);
      }
      return { value, max };
    }
    if (this.isArcaneDevice) {
      return getProperty(this.system, 'powerPoints') as ItemChatCardPowerPoints;
    }
    return null;
  }

  protected override async _preCreate(
    data: ItemDataConstructorData,
    options: DocumentModificationOptions,
    user: User,
  ) {
    await super._preCreate(data, options, user);
    //Set default image if no image already exists
    if (!data.img) {
      this.updateSource({
        img: `systems/swade/assets/icons/${data.type}.svg`,
      });
    }

    if (this.parent) {
      if (data.type === 'skill' && options.renderSheet !== null) {
        options.renderSheet = true;
      }
      if (
        this.parent.type === 'npc' &&
        hasProperty(this.system, 'equippable')
      ) {
        let newState: EquipState = constants.EQUIP_STATE.EQUIPPED;
        if (data.type === 'weapon') {
          newState = constants.EQUIP_STATE.MAIN_HAND;
        }
        this.updateSource({
          data: {
            equipStatus: newState,
          },
        });
      }
    }
  }

  protected override async _preDelete(options, user: User) {
    await super._preDelete(options, user);
    //delete all transferred active effects from the actor
    if (this.parent) {
      const toDelete = this.parent.effects
        .filter((e) => e.origin === this.uuid)
        .map((ae) => ae.id!);
      await this.parent.deleteEmbeddedDocuments('ActiveEffect', toDelete);
    }
  }

  protected override async _preUpdate(changed, options, user) {
    await super._preUpdate(changed, options, user);

    if (this.parent && hasProperty(changed, 'system.equipStatus')) {
      //toggle all active effects when an item equip status changes
      const newState = getProperty(changed, 'system.equipStatus') as EquipState;
      const updates = this.parent.effects
        .filter((ae) => ae.origin === this.uuid)
        .map((ae) => {
          return {
            _id: ae.id,
            disabled: newState < constants.EQUIP_STATE.EQUIPPED,
          };
        });
      await this.parent.updateEmbeddedDocuments('ActiveEffect', updates);
    }
  }
}
